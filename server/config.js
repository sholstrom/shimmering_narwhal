const config = {
  mongoURL: process.env.MONGO_URL || 'mongodb://basementbebop:starscream69@ds231568.mlab.com:31568/narwhal',
  port: process.env.PORT || 8000,
};

export default config;
